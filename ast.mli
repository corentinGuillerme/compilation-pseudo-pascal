type id = 
    | Id of string

type constant = 
    | Int   of int
    | Bool  of bool

type pascalType =
    | Integer
    | Boolean
    | Array     of pascalType

type environment = 
    | Environment   of (id list * pascalType) list

type arithmeticOperator =
    | Plus
    | Times
    | Minus
    | Divide

type comparaisonOperator =
    | GT
    | LT
    | GE
    | LE
    | EQ
    | NE
    
type expression =
    | VariableExpression        of id
    | ConstantExpression        of constant
    | MinusExpression           of expression
    | ArithmeticExpression      of expression * arithmeticOperator * expression
    | ComparaisonExpression     of expression * comparaisonOperator * expression
    | FunctionCallExpression    of id * expression list
    | ArrayAccessExpression     of expression * expression
    | NewArrayExpression        of pascalType * expression
    

type condition =
    | Expression    of expression
    | Not           of condition
    | Or            of condition * condition
    | And           of condition * condition

type statement = 
    | ProcedureCallStatement    of id * expression list
    | AssignVarStatement        of id * expression
    | AssignArrayStatement      of expression * expression * expression
    | IfStatement               of condition * statement * statement
    | WhileStatement            of condition * statement
    | BlocStatement             of statement list
    
type declaration =
    | FunctionDeclaration   of id * environment * pascalType * declaration list * statement
    | ProcedureDeclaration  of id * environment * declaration list * statement
    | VarDeclaration        of environment

type program = 
    | Program   of declaration list * statement

val print : program -> unit