type op = string (* opérateur MIPS *)

type functionId = string (* identifiant de fonction *)

type cte = int (* constante entière *)

type label = int (* label *)

type arg = (* arguments pour les opérateurs MIPS *)
  | Null                      (* pas d'argument *)
  | PseudoReg   of int        (* pseudo registre *)
  | Cte         of int        (* constante entière *)
  | Address     of int * int  (* addresse *)

type node = (* noeud *)
  | Node               of op * arg * arg * arg        (* noeud pour un opérateur MIPS avec trois arguments *)
  | FunctionCallNode   of arg * functionId * arg list (* noeud pour un appel de fonction/procédure avec une liste d'arguments *)

type rtl_node = (* noeud rtl composé d'un noeud et d'un ou deux labels de sortie *)
  | RTLNode               of node * label * label option

module type Int = (* module pour les entiers *)
sig
  type t
  val compare : t -> t -> int
end

type rtl_graph

type rtl_function 

(* crée le graphe rtl à partir du programme Ast.Program(...) *)
val build_rtl : Ast.program -> rtl_function list

(* affiche la fonction rtl FunctionRTL(...) *)
val print_rtl_function : rtl_function -> unit

(* affiche le graphe rtl l *)
val print_rtl : rtl_function list -> unit