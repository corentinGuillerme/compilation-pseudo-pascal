type id = 
    | Id of string

type constant = 
    | Int   of int
    | Bool  of bool

type pascalType =
    | Integer
    | Boolean
    | Array     of pascalType

type environment = 
    | Environment   of (id list * pascalType) list

type arithmeticOperator =
    | Plus
    | Times
    | Minus
    | Divide

type comparaisonOperator =
    | GT
    | LT
    | GE
    | LE
    | EQ
    | NE
    
type expression =
    | VariableExpression        of id
    | ConstantExpression        of constant
    | MinusExpression           of expression
    | ArithmeticExpression      of expression * arithmeticOperator * expression
    | ComparaisonExpression     of expression * comparaisonOperator * expression
    | FunctionCallExpression    of id * expression list
    | ArrayAccessExpression     of expression * expression
    | NewArrayExpression        of pascalType * expression
    

type condition =
    | Expression    of expression
    | Not           of condition
    | Or            of condition * condition
    | And           of condition * condition

type statement = 
    | ProcedureCallStatement    of id * expression list
    | AssignVarStatement        of id * expression
    | AssignArrayStatement      of expression * expression * expression
    | IfStatement               of condition * statement * statement
    | WhileStatement            of condition * statement
    | BlocStatement             of statement list
    
type declaration =
    | FunctionDeclaration   of id * environment * pascalType * declaration list * statement
    | ProcedureDeclaration  of id * environment * declaration list * statement
    | VarDeclaration        of environment

type program = 
    | Program   of declaration list * statement

let rec f i = g 1
and g i = f 2

let rec print_sep l =
    List.iter print_string l
    
and print_sep_spec l =
    match l with
    | [] -> ()
    | [x] -> print_string "|--"
    | x :: q -> print_string x; print_sep_spec q

and print_id l a = 
    print_sep_spec l;
    match a with
    | Id s -> Printf.printf "Id(%s)\n" s

and print_constant l a = 
    print_sep_spec l;
    match a with
    | Int i -> Printf.printf "Int(%i)\n" i
    | Bool b -> Printf.printf "Bool(%b)\n" b

and print_arithmeticOperator l a = 
    print_sep_spec l;
    match a with
    | Plus -> Printf.printf "Plus\n"
    | Times -> Printf.printf "Times\n"
    | Minus -> Printf.printf "Minus\n"
    | Divide -> Printf.printf "Divide\n"

and print_comparaisonOperator l a = 
    print_sep_spec l;
    match a with   
    | GT -> Printf.printf "GT\n"
    | LT -> Printf.printf "LT\n"
    | LE -> Printf.printf "LE\n"
    | GE -> Printf.printf "GE\n"
    | NE -> Printf.printf "NE\n"
    | EQ -> Printf.printf "EQ\n"

and print_pascalType l a = 
    let rec aux a =
        match a with
        | Integer -> print_string "integer"
        | Boolean -> print_string "boolean"
        | Array(t) -> print_string "array of "; aux t
    in
    print_sep_spec l;
    aux a;
    print_newline ()

and print_idList l a =
    match a with
    | [] -> print_sep l; print_string "\n"
    | [t] -> print_id (l @ ["|  "]) t

    | t::q -> 
        print_id (l @ ["|  "]) t;
        print_sep (l @ ["|\n"]);
        print_idList (l) q

and print_environment l (Environment(a)) =
    let rec aux l a =
        match a with
        | [] -> print_sep l; print_string "\n"
        | [idList, pascalType] ->
            print_sep_spec l;
            print_string "(id list * pascalType)\n";
            print_sep (l @ ["|\n"]);
            print_sep_spec (l @ ["|  "]);
            print_string "id list\n";
            print_sep (l @ ["|  |\n"]);
            print_idList (l @ ["|  "]) idList;
            print_sep (l @ ["|\n"]);
            print_pascalType (l @ ["|  "]) pascalType;

        | (idList, pascalType)::q -> 
            print_sep_spec l;
            print_string "(id list * pascalType)\n";
            print_sep (l @ ["|\n"]);
            print_sep_spec (l @ ["|  "]);
            print_string "id list\n";
            print_sep (l @ ["|  |\n"]);
            print_idList (l @ ["|  "]) idList;
            print_sep (l @ ["|\n"]);
            print_pascalType (l @ ["|  "]) pascalType;
            print_sep (List.tl l @ ["|\n"]);
            aux (l) q
    in
    print_sep_spec l;
    print_string "Environment\n";
    print_sep (l @ ["|\n"]);
    aux (l @ ["|  "]) a


and print_declaration l a =
    match a with
    | FunctionDeclaration(id, environment, pascalType, declList, statement) ->
        print_string "FunctionDeclaration\n";
        print_sep (l @ ["|\n"]);
        print_id (l @ ["|  "]) id;
        print_sep (l @ ["|\n"]);
        print_environment (l @ ["|  "]) environment;
        print_sep (l @ ["|\n"]);
        print_pascalType (l @ ["|  "]) pascalType;
        print_sep (l @ ["|\n"]);
        print_sep_spec (l @ ["|  "]);
        print_string "declaration list\n|  ";
        print_sep (l @ ["|\n|  "]);
        print_declList (l @ ["|  "]) declList;
        print_sep (l @ ["|\n"]);
        print_sep_spec l;
        print_statement (l @ ["|  "]) statement

    | ProcedureDeclaration(id, environment, declList, statement) ->
        print_string "ProcedureDeclaration\n";
        print_sep (l @ ["|\n"]);
        print_id (l @ ["|  "]) id;
        print_sep (l @ ["|\n"]);
        print_environment (l @ ["|  "]) environment;
        print_sep (l @ ["|\n"]);
        print_sep_spec (l @ ["|  "]);
        print_string "declaration list\n|  ";
        print_sep (l @ ["|\n|  "]);
        print_declList (l @ ["|  "]) declList;
        print_sep (l @ ["|\n"]);
        print_sep_spec l;
        print_statement (l @ ["|  "]) statement

    | VarDeclaration(environment) ->
        print_string "VarDeclaration\n";
        print_sep (l @ ["|\n"]);
        print_environment (l @ ["|  "]) environment;

and print_declList l a = 
    match a with
    | [] -> print_sep l; print_string "\n"
    | [t] -> print_sep_spec l;
        print_declaration (l @ ["|  "]) t

    | t::q -> 
        print_sep_spec l;
        print_declaration (l @ ["|  "]) t;
        print_sep (l @ ["|\n"]);
        print_sep (l);
        print_declList (l) q

and print_expression l a =
    print_sep_spec l;
    match a with
    | VariableExpression(id) ->
        print_string "VariableExpression\n";
        print_sep (l @ ["|\n"]);
        print_id (l @ ["|  "]) id

    | ConstantExpression(cte) ->
        print_string "ConstantExpression\n";
        print_sep (l @ ["|\n"]);
        print_constant (l @ ["|  "]) cte

    | MinusExpression(expr) ->
        print_string "MinusExpression\n";
        print_sep (l @ ["|\n"]);
        print_expression (l @ ["|  "]) expr

    | ArithmeticExpression(expr1, op, expr2) ->
        print_string "ArithmeticExpression\n";
        print_sep (l @ ["|\n"]);
        print_expression (l @ ["|  "]) expr1;
        print_sep (l @ ["|\n"]);
        print_arithmeticOperator (l @ ["|  "]) op;
        print_sep (l @ ["|\n"]);
        print_expression (l @ ["|  "]) expr2

    | ComparaisonExpression(expr1, op, expr2) ->
        print_string "ArithmeticExpression\n";
        print_sep (l @ ["|\n"]);
        print_expression (l @ ["|  "]) expr1;
        print_sep (l @ ["|\n"]);
        print_comparaisonOperator (l @ ["|  "]) op;
        print_sep (l @ ["|\n"]);
        print_expression (l @ ["|  "]) expr2

    | FunctionCallExpression(id, exprList) ->
        print_string "FunctionCallExpression\n";
        print_sep (l @ ["|\n"]);
        print_id (l @ ["|  "]) id;
        print_sep (l @ ["|\n"]);
        print_expressionList (l @ ["|  "]) exprList

    | ArrayAccessExpression(expr1, expr2) ->
        print_string "ArrayAccessExpression\n";
        print_sep (l @ ["|\n"]);
        print_expression (l @ ["|  "]) expr1;
        print_sep (l @ ["|\n"]);
        print_expression (l @ ["|  "]) expr2

    | NewArrayExpression(pascalType, expr) ->
        print_string "NewArrayExpression\n";
        print_sep (l @ ["|\n"]);
        print_pascalType (l @ ["|  "]) pascalType;
        print_sep (l @ ["|\n"]);
        print_expression (l @ ["|  "]) expr

and print_expressionList l a =
    print_sep_spec (l);
    print_string "ExpressionList\n"

and print_condition l a =
    print_sep_spec l;
    match a with
    | Expression(expr) ->
        print_string "Expression\n";
        print_sep (l @ ["|\n"]);
        print_expression (l @ ["|  "]) expr
    
    | Not(cond) ->
        print_string "Not\n";
        print_sep (l @ ["|\n"]);
        print_condition (l @ ["|  "]) cond

    | Or(cond1, cond2) ->
        print_string "Or\n";
        print_sep (l @ ["|\n"]);
        print_condition (l @ ["|  "]) cond1;
        print_sep (l @ ["|\n"]);
        print_condition (l @ ["|  "]) cond2

    | And(cond1, cond2) ->
        print_string "And\n";
        print_sep (l @ ["|\n"]);
        print_condition (l @ ["|  "]) cond1;
        print_sep (l @ ["|\n"]);
        print_condition (l @ ["|  "]) cond2

and print_statement l a =
    match a with
    | ProcedureCallStatement(id, expressions) -> 
        print_string "ProcedureCallStatement\n";
        print_sep (l @ ["|\n"]);
        print_id (l @ ["|\n"]) id;
        print_sep (l @ ["|\n"]);
        print_expressionList (l @ ["|  "]) expressions

    | AssignVarStatement(id, expression) ->
        print_string "AssignVarStatement\n";
        print_sep (l @ ["|\n"]);
        print_id (l @ ["|\n"]) id;
        print_sep (l @ ["|\n"]);
        print_expression (l @ ["|  "]) expression

    | AssignArrayStatement(expr1, expr2, expr3) ->
        print_string "AssignArrayStatement\n";
        print_sep (l @ ["|\n"]);
        print_expression (l @ ["|  "]) expr1;
        print_sep (l @ ["|\n"]);
        print_expression (l @ ["|  "]) expr2;
        print_sep (l @ ["|\n"]);
        print_expression (l @ ["|  "]) expr3

    | IfStatement(cond, statement1, statement2) ->
        print_string "IfStatement\n";
        print_sep (l @ ["|\n"]);
        print_condition (l @ ["|  "]) cond;
        print_sep (l @ ["|\n"]);
        print_sep_spec (l @ ["|  "]);
        print_statement (l @ ["|  "]) statement1;
        print_sep (l @ ["|\n"]);
        print_sep_spec (l @ ["|  "]);
        print_statement (l @ ["|  "]) statement2

    | WhileStatement(cond, statement) ->
        print_string "WhileStatement\n";
        print_sep (l @ ["|\n"]);
        print_condition (l @ ["|  "]) cond;
        print_sep (l @ ["|\n"]);
        print_sep_spec (l @ ["|  "]);
        print_statement (l @ ["|  "]) statement

    | BlocStatement(statements) -> 
        print_string "BlocStatement\n";
        print_sep (l @ ["|\n"]);
        print_statementList (l) statements

and print_statementList l a = 
    match a with
    | [] -> print_sep (l @ ["|  "]); print_string "\n"
    | [t] -> print_sep_spec (l @ ["|  "]);
        print_statement (l @ ["|  "]) t

    | t::q -> 
        print_sep_spec (l @ ["|  "]);
        print_statement (l @ ["|  "]) t;
        print_sep (l @ ["|\n"]);
        print_statementList (l) q

and print_program l a =
    match a with
    | Program(declList, statement) -> 
        print_string "Program\n";
        print_sep (l @ ["|\n"]);
        print_sep_spec (l @ ["|  "]);
        print_string "declaration list\n|  ";
        print_sep (l @ ["|\n|  "]);
        print_declList (l @ ["|  "]) declList;
        print_sep (l @ ["|\n"]);
        print_sep_spec l;
        print_statement (l @ ["|  "]) statement
    


let print a = print_program [] a


