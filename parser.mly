%{
  open Ast
%}

%token PLUS MINUS TIMES DIVIDE LT GT LE GE EQ NE INT BOOL_CTE ID INTTYPE BOOLTYPE ARRAYTYPE ASSIGN IF THEN ELSE WHILE DO BEGIN END PROGRAM FUNCTION PROCEDURE VAR COMMENT FULLSTOP COMMA SEMICOLON COLON OPENPARENTHESES CLOSEPARENTHESES OPENSQUAREBRACKETS CLOSESQUAREBRACKETS AND OR NOT NEW
%left OR
%left AND
%nonassoc GT GE LE LT NE EQ
%left PLUS MINUS
%left TIMES DIVIDE
%left NOT
%nonassoc OPENSQUAREBRACKETS
%token<int> INT_CTE
%token<bool> BOOL_CTE
%token<string> ID
%start s
%type<Ast.program> s
%%

s :
  PROGRAM def_list bloc_statement FULLSTOP {Program($2, $3)}

def_list :
  | def def_list {$1 :: $2}
  | {[]}

def :
  | VAR non_empty_environment {VarDeclaration($2)}
  | PROCEDURE id OPENPARENTHESES environment CLOSEPARENTHESES SEMICOLON def_list bloc_statement SEMICOLON {ProcedureDeclaration($2, $4, $7, $8)}
  | FUNCTION id OPENPARENTHESES environment CLOSEPARENTHESES COLON pascal_type SEMICOLON def_list bloc_statement SEMICOLON {FunctionDeclaration($2, $4, $7, $9, $10)}

statement :
  | id OPENPARENTHESES expression_list CLOSEPARENTHESES {ProcedureCallStatement($1, $3)}
  | id ASSIGN expression {AssignVarStatement($1, $3)}
  | expression OPENSQUAREBRACKETS expression CLOSESQUAREBRACKETS ASSIGN expression {AssignArrayStatement($1, $3, $6)}
  | IF condition THEN statement ELSE statement {IfStatement($2, $4, $6)}
  | WHILE condition DO statement {WhileStatement($2, $4)}
  | bloc_statement {$1}

bloc_statement :
  | BEGIN statement_list END {BlocStatement($2)}

non_empty_statement_list :
  | statement SEMICOLON non_empty_statement_list {$1 :: $3}
  | statement {[$1]}

statement_list :
  | non_empty_statement_list {$1}
  | {[]}

condition :
  | expression {Expression($1)}
  | non_expression_condition {$1}

non_expression_condition :
  | NOT condition {Not($2)}
  | condition AND condition {And($1, $3)}
  | condition OR condition {Or($1, $3)}
  | OPENPARENTHESES non_expression_condition CLOSEPARENTHESES {$2}

non_empty_environment_aux :
  | non_empty_id_list COLON pascal_type SEMICOLON non_empty_environment_aux {($1, $3) :: $5}
  | non_empty_id_list COLON pascal_type SEMICOLON {[($1, $3)]}

non_empty_environment :
  | non_empty_environment_aux {Environment($1)}

environment_aux :
  | non_empty_id_list COLON pascal_type SEMICOLON environment_aux {($1, $3) :: $5}
  | non_empty_id_list COLON pascal_type {[($1, $3)]}

environment :
  | environment_aux {Environment($1)}
  | {Environment([])}

pascal_type :
  | INTTYPE {Integer}
  | BOOLTYPE {Boolean}
  | ARRAYTYPE pascal_type {Array($2)}

non_empty_id_list :
  | id COMMA id_list {$1 :: $3}
  | id {[$1]}

id_list :
  | non_empty_id_list {$1}
  | {[]}

non_empty_expression_list :
  | expression COMMA expression_list {$1 :: $3}
  | expression {[$1]}

expression_list :
  | non_empty_expression_list {$1}
  | {[]}

expression :
  | constant_expression {ConstantExpression($1)}
  | id {VariableExpression($1)}
  | MINUS expression {MinusExpression($2)}
  | expression GT expression {ComparaisonExpression($1, GT, $3)}
  | expression LT expression {ComparaisonExpression($1, LT, $3)}
  | expression GE expression {ComparaisonExpression($1, GE, $3)}
  | expression LE expression {ComparaisonExpression($1, LE, $3)}
  | expression EQ expression {ComparaisonExpression($1, EQ, $3)}
  | expression NE expression {ComparaisonExpression($1, NE, $3)}
  | expression PLUS expression {ArithmeticExpression($1, Plus, $3)}
  | expression TIMES expression {ArithmeticExpression($1, Times, $3)}
  | expression MINUS expression {ArithmeticExpression($1, Minus, $3)}
  | expression DIVIDE expression {ArithmeticExpression($1, Divide, $3)}
  | id OPENPARENTHESES expression_list CLOSEPARENTHESES {FunctionCallExpression($1, $3)}
  | OPENPARENTHESES expression CLOSEPARENTHESES {$2}
  | expression OPENSQUAREBRACKETS expression CLOSESQUAREBRACKETS {ArrayAccessExpression($1, $3)}
  | NEW ARRAYTYPE pascal_type OPENSQUAREBRACKETS expression CLOSESQUAREBRACKETS {NewArrayExpression($3, $5)}

constant_expression :
  | INT_CTE {Int($1)}
  | BOOL_CTE {Bool($1)}

id :
  | ID {Id($1)}