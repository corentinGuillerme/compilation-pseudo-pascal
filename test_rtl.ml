let fileArray = Sys.argv

let n = ref 0

let _ = 
for i = 1 to -1 + Array.length fileArray do
  try
    let a = Parser.s (Lexer.decoupe) (Lexing.from_channel (open_in (Array.get fileArray i))) in
    let _ = Rtl.build_rtl a in
    Printf.printf "%s : success\n" (Array.get fileArray i);
    n := !n + 1
  with error -> Printf.printf "%s : fail %s\n" (Array.get fileArray i) (Printexc.to_string error)
done

let _ = Printf.printf "%i success / %i \n" !n (-1 + Array.length fileArray)