all: build_rtl

.PHONY : run_tests
run_tests : test_parser test_rtl
	./test_parser $(shell ls test_files/*)
	./test_rtl $(shell ls test_files/*)

.PHONY : clean
clean:
	rm -f lexer.ml parser.ml parser.mli *.cmo *.cmi

.PHONY: clear_all
clear_all: clean
	rm -f parser_pseudo_pascal test_parser build_rtl test_rtl

parser_pseudo_pascal: ast.cmo parser.cmo lexer.cmo parser_pseudo_pascal.cmo
	ocamlc -o $@ $^

test_parser: ast.cmo parser.cmo lexer.cmo test_parser.cmo
	ocamlc -o $@ $^

build_rtl: ast.cmo parser.cmo lexer.cmo rtl.cmo build_rtl.cmo
	ocamlc -o $@ $^

test_rtl: ast.cmo parser.cmo lexer.cmo rtl.cmo test_rtl.cmo
	ocamlc -o $@ $^


%.cmi: %.mli
	ocamlc -c $<

%.cmo: %.ml
	ocamlc -c $<



lexer.ml: lexer.mll
	ocamllex $<
parser.ml: parser.mly
	ocamlyacc $<
parser.cmo: ast.cmi parser.cmi
parser.cmi: parser.mli ast.cmo
	ocamlc -c $<
ast.cmo: ast.cmi

parser_pseudo_pascal.cmo: parser.cmi lexer.cmo ast.cmi

rtl.cmo: ast.cmi rtl.cmi
build_rtl.cmo: ast.cmi rtl.cmi
test_rtl.cmo: ast.cmi rtl.cmi