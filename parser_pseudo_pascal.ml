let lexbuf = Lexing.from_channel stdin

let _ =
  let a = Parser.s (Lexer.decoupe) lexbuf in
  Ast.print a;
  print_newline ()