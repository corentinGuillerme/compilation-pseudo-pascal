let lexbuf = Lexing.from_channel (open_in (Array.get Sys.argv 1))

let _ =
  let a = Parser.s (Lexer.decoupe) lexbuf in
  let b = Rtl.build_rtl a in
  Rtl.print_rtl b