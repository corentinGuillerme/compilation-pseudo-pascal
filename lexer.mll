{
    open Parser
}

rule decoupe = parse
    | ":=" {ASSIGN}

    | "+" {PLUS}
    | "-" {MINUS}
    | "*" {TIMES}
    | "/" {DIVIDE}

    | "<" {LT}
    | ">" {GT}
    | "<=" {LE}
    | ">=" {GE}
    | "=" {EQ}
    | "<>" {NE}

    | "integer" {INTTYPE}
    | "boolean" {BOOLTYPE}
    | "array of" {ARRAYTYPE}
    | "new" {NEW}

    | "and" {AND}
    | "or" {OR}
    | "not" {NOT}

    | "if" {IF}
    | "then" {THEN}
    | "else" {ELSE}

    | "while" {WHILE}
    | "do" {DO}

    | "begin" {BEGIN}
    | "end" {END}

    | "program" {PROGRAM}
    | "function" {FUNCTION}
    | "procedure" {PROCEDURE}

    | "var" {VAR}

    | '{'[^'}']*'}' {decoupe lexbuf}

    | '.' {FULLSTOP}
    | ';' {SEMICOLON}
    | ':' {COLON}
    | ',' {COMMA}
    | '(' {OPENPARENTHESES}
    | ')' {CLOSEPARENTHESES}
    | '[' {OPENSQUAREBRACKETS}
    | ']' {CLOSESQUAREBRACKETS}

    | [' ''\t''\n']+ {decoupe lexbuf}

    | ['0'-'9']+ {INT_CTE (int_of_string (Lexing.lexeme lexbuf))}
    | "true"|"false" {BOOL_CTE (bool_of_string (Lexing.lexeme lexbuf))}
    | ['a'-'z''A'-'Z']['a'-'z''A'-'Z''0'-'9']* {ID (Lexing.lexeme lexbuf)}