type op = string (* opérateur MIPS *)

type functionId = string (* identifiant de fonction *)

type cte = int (* constante entière *)

type label = int (* label *)

type arg = (* arguments pour les opérateurs MIPS *)
  | Null                      (* pas d'argument *)
  | PseudoReg   of int        (* pseudo registre *)
  | Cte         of int        (* constante entière *)
  | Address     of int * int  (* addresse *)

type node = (* noeud *)
  | Node               of op * arg * arg * arg        (* noeud pour un opérateur MIPS avec trois arguments *)
  | FunctionCallNode   of arg * functionId * arg list (* noeud pour un appel de fonction/procédure avec une liste d'arguments *)

type rtl_node = (* noeud rtl composé d'un noeud et d'un ou deux labels de sortie *)
  | RTLNode               of node * label * label option

module type Int = (* module pour les entiers *)
sig
  type t
  val compare : t -> t -> int
end

module Int = (* module pour les entiers *)
struct
  type t = int
  let compare a b = a-b
end

module RTL = Map.Make(Int) (* module pour les graphes rtl *)

module Id = (* module pour les identifiants *)
struct
  type t = Ast.id
  let compare = compare
end

module Dict = Map.Make(Id) (* module pour les dictionnaires identifiant-entier *)

type pseudo_reg_memory = {  (* structure contenant les registres utilisés *)
  vars : int Dict.t;        (* dictionnaire avec la correspondance identifiant-registre *)
  max_reg : int             (* plus grand registre utilisé *)
}

type rtl_graph = rtl_node RTL.t (* type pour les graphes rtl *)

type rtl_function = FunctionRTL of functionId * arg list * arg option * arg list * label * label * rtl_graph (* type pour les fonctions/procédures rtl *)

(* affiche le dictionnaire dict *)
let print_dict dict =
  Dict.fold (fun (Ast.Id(k)) reg _ -> Printf.printf "%s -> %i\n" k reg) dict ()

(* crée un nouveau registre et renvoie reg mis à jour et le nouveau registre *)
let new_register reg =
  let n = 1+reg.max_reg in
  {vars= Dict.add (Ast.Id(string_of_int n)) n reg.vars; max_reg= n}, n

(* ajoute node à la fin de rtl *)
let add_node node rtl =
  let n_line = if RTL.is_empty rtl
    then 0
    else 1 + fst (RTL.max_binding rtl)
  in RTL.add n_line (RTLNode(node, n_line+1, None)) rtl, n_line

(* ajoute node à la ligne n_line de rtl avec les labels de sortie label1 et label2 *)
let add_rtl_node n_line node label1 label2 rtl =
  RTL.add n_line (RTLNode(node, label1, label2)) rtl, n_line

(* ajoute les identifiants de env à dict *)
let rec init_dict env dict =
  match env with
  | [] -> dict
  | (a, _)::b -> 
    init_dict b (List.fold_left 
                   (fun pseudo_reg k -> 
                      if Dict.mem k dict
                      then pseudo_reg
                      else Dict.add k ( 1 + (Dict.cardinal pseudo_reg)) pseudo_reg
                   ) dict a)

(* retourne la liste des registres contenus dans dict *)
let get_reg_from_dict dict =
  Dict.fold (fun _ reg l -> l@[PseudoReg reg]) dict []

(* affiche les registres contenus dans l *)
let rec print_reg_list l =
  match l with
  | [] -> ()
  | [PseudoReg(a)] -> Printf.printf "%%%i" a
  | PseudoReg(a)::b -> Printf.printf "%%%i, " a; print_reg_list b
  | a::b -> print_reg_list b

(* retourne la liste des registres correspondant aux identifiants de env avec dict *)
let rec get_env_reg env dict =
  match env with
  | [] -> []
  | (a, _)::b -> 
    (List.map 
       (fun pseudo_reg -> PseudoReg(Dict.find pseudo_reg dict)

       ) a)@(get_env_reg b dict)

(* retourne (k, n) tel que n est le plus grand entier tel que a = k*2^n *)
let log2 a =
  let rec aux i res =
    if i mod 2 = 0
    then aux (i/2) (res+1)
    else res, i

  in 
  if a = 0
  then 0,0
  else aux a 0








(* simplifie l'expression expr *)
let rec reduce_expression expr =
  match expr with
  | Ast.VariableExpression(id) ->
    expr

  | Ast.ConstantExpression(cte) -> 
    expr

  | Ast.MinusExpression(expr1) -> 
    let expr1' = reduce_expression expr1 in

    (match expr1' with
     | Ast.ConstantExpression(Ast.Int(c)) -> Ast.ConstantExpression(Ast.Int(-1*c))
     | Ast.MinusExpression(e) -> e
     | _ -> Ast.MinusExpression(expr1')
    )

  | Ast.ArithmeticExpression(expr1, Ast.Plus, expr2) -> 
    let expr1' = reduce_expression expr1 in
    let expr2' = reduce_expression expr2 in

    (match expr1', expr2' with
     | Ast.ConstantExpression(Ast.Int(c1)), Ast.ConstantExpression(Ast.Int(c2)) ->
       Ast.ConstantExpression(Ast.Int(c1+c2))

     | Ast.MinusExpression(e), _ when e = expr2' ->
       Ast.ConstantExpression(Ast.Int(0))

     | _, Ast.MinusExpression(e) when e = expr1' ->
       Ast.ConstantExpression(Ast.Int(0))

     | Ast.ConstantExpression(Ast.Int(0)), _ ->
       expr2'

     | _, Ast.ConstantExpression(Ast.Int(0)) ->
       expr1'

     | Ast.ConstantExpression(Ast.Int(c1)), e ->
       Ast.ArithmeticExpression(expr2', Ast.Plus, expr1')

     | _ -> Ast.ArithmeticExpression(expr1', Ast.Plus, expr2')
    )

  | Ast.ArithmeticExpression(expr1, Ast.Minus, expr2) -> 
    let expr1' = reduce_expression expr1 in
    let expr2' = reduce_expression expr2 in

    (match expr1', expr2' with
     | Ast.ConstantExpression(Ast.Int(c1)), Ast.ConstantExpression(Ast.Int(c2)) ->
       Ast.ConstantExpression(Ast.Int(c1-c2))

     | _ when expr1' = expr2' ->
       Ast.ConstantExpression(Ast.Int(0))

     | _, Ast.ConstantExpression(Ast.Int(0)) ->
       expr1'

     | _ -> Ast.ArithmeticExpression(expr1', Ast.Minus, expr2')
    )

  | Ast.ArithmeticExpression(expr1, Ast.Divide, expr2) -> 
    let expr1' = reduce_expression expr1 in
    let expr2' = reduce_expression expr2 in

    Ast.ArithmeticExpression(expr1', Ast.Divide, expr2')

  | Ast.ArithmeticExpression(expr1, Ast.Times, expr2) -> 
    let expr1' = reduce_expression expr1 in
    let expr2' = reduce_expression expr2 in

    (match expr1', expr2' with
     | Ast.ConstantExpression(Ast.Int(c1)), Ast.ConstantExpression(Ast.Int(c2)) ->
       Ast.ConstantExpression(Ast.Int(c1*c2))

     | Ast.ConstantExpression(Ast.Int(0)), _ ->
       Ast.ConstantExpression(Ast.Int(0))

     | _, Ast.ConstantExpression(Ast.Int(0)) ->
       Ast.ConstantExpression(Ast.Int(0))

     | Ast.ConstantExpression(Ast.Int(1)), _ ->
       expr2'

     | _, Ast.ConstantExpression(Ast.Int(1)) ->
       expr1'

     | Ast.ConstantExpression(Ast.Int(c1)), e ->
       Ast.ArithmeticExpression(expr2', Ast.Times, expr1')

     | _ -> Ast.ArithmeticExpression(expr1', Ast.Times, expr2')
    )



  | Ast.ComparaisonExpression(expr1, op, expr2) ->

    let expr1' = reduce_expression expr1 in
    let expr2' = reduce_expression expr2 in

    (match op, expr1', expr2' with
     | Ast.GT, Ast.ConstantExpression(Ast.Int(c1)), Ast.ConstantExpression(Ast.Int(c2)) -> Ast.ConstantExpression(Ast.Bool(c1>c2))
     | Ast.LT, Ast.ConstantExpression(Ast.Int(c1)), Ast.ConstantExpression(Ast.Int(c2)) -> Ast.ConstantExpression(Ast.Bool(c1<c2))
     | Ast.GE, Ast.ConstantExpression(Ast.Int(c1)), Ast.ConstantExpression(Ast.Int(c2)) -> Ast.ConstantExpression(Ast.Bool(c1>=c2))
     | Ast.LE, Ast.ConstantExpression(Ast.Int(c1)), Ast.ConstantExpression(Ast.Int(c2)) -> Ast.ConstantExpression(Ast.Bool(c1<=c2))
     | Ast.EQ, Ast.ConstantExpression(Ast.Int(c1)), Ast.ConstantExpression(Ast.Int(c2)) -> Ast.ConstantExpression(Ast.Bool(c1=c2))
     | Ast.NE, Ast.ConstantExpression(Ast.Int(c1)), Ast.ConstantExpression(Ast.Int(c2)) -> Ast.ConstantExpression(Ast.Bool(c1<>c2))
     | _ -> Ast.ComparaisonExpression(expr1', op, expr2')
    )

  | Ast.FunctionCallExpression(Ast.Id(id), expr_list) -> 
    Ast.FunctionCallExpression(Ast.Id(id), List.map reduce_expression expr_list)

  | Ast.ArrayAccessExpression(expr1, expr2) ->
    let expr1' = reduce_expression expr1 in
    let expr2' = reduce_expression expr2 in

    Ast.ArrayAccessExpression(expr1', expr2')

  | Ast.NewArrayExpression(t, expr1) -> 
    let expr1' = reduce_expression expr1 in
    Ast.NewArrayExpression(t, expr1')

(* simplifie la condition cond *)
let rec reduce_condition cond = 
  match cond with
  | Ast.Expression(expr) ->
    let expr' = reduce_expression expr in

    Ast.Expression(expr')

  | Ast.Not(cond1) ->
    let cond1' = reduce_condition cond1 in

    (match cond1' with
     | Ast.Not(c) -> c

     | Ast.Expression(Ast.ConstantExpression(Ast.Bool(e))) ->
       Ast.Expression(Ast.ConstantExpression(Ast.Bool(not e)))

     | Ast.Expression(Ast.ComparaisonExpression(expr1, Ast.NE, expr2)) ->
       Ast.Expression(Ast.ComparaisonExpression(expr1, Ast.EQ, expr2))

     | Ast.Expression(Ast.ComparaisonExpression(expr1, Ast.EQ, expr2)) ->
       Ast.Expression(Ast.ComparaisonExpression(expr1, Ast.NE, expr2))

     | Ast.Expression(Ast.ComparaisonExpression(expr1, Ast.GT, expr2)) ->
       Ast.Expression(Ast.ComparaisonExpression(expr1, Ast.LE, expr2))

     | Ast.Expression(Ast.ComparaisonExpression(expr1, Ast.GE, expr2)) ->
       Ast.Expression(Ast.ComparaisonExpression(expr1, Ast.LT, expr2))

     | Ast.Expression(Ast.ComparaisonExpression(expr1, Ast.LT, expr2)) ->
       Ast.Expression(Ast.ComparaisonExpression(expr1, Ast.GE, expr2))

     | Ast.Expression(Ast.ComparaisonExpression(expr1, Ast.LE, expr2)) ->
       Ast.Expression(Ast.ComparaisonExpression(expr1, Ast.GT, expr2))

     | _ -> Ast.Not(cond1'))

  | Ast.Or(cond1, cond2) ->
    let cond1' = reduce_condition cond1 in
    let cond2' = reduce_condition cond2 in

    (match cond1',cond2' with
     | Ast.Expression(Ast.ConstantExpression(Ast.Bool(c1))), Ast.Expression(Ast.ConstantExpression(Ast.Bool(c2))) -> Ast.Expression(Ast.ConstantExpression(Ast.Bool(c1 || c2)))
     | c1, Ast.Not(c2) when c1=c2-> Ast.Expression(Ast.ConstantExpression(Ast.Bool(true)))
     | Ast.Not(c1), c2 when c1=c2-> Ast.Expression(Ast.ConstantExpression(Ast.Bool(true)))
     | Ast.Expression(Ast.ConstantExpression(Ast.Bool(true))), _ -> Ast.Expression(Ast.ConstantExpression(Ast.Bool(true)))
     | _, Ast.Expression(Ast.ConstantExpression(Ast.Bool(true))) -> Ast.Expression(Ast.ConstantExpression(Ast.Bool(true)))
     | Ast.Expression(Ast.ConstantExpression(Ast.Bool(false))), _ -> cond2'
     | _, Ast.Expression(Ast.ConstantExpression(Ast.Bool(false))) -> cond1'
     | _ when cond1'=cond2'-> cond1'
     | _ -> Ast.Or(cond1', cond2'))

  | Ast.And(cond1, cond2) ->
    let cond1' = reduce_condition cond1 in
    let cond2' = reduce_condition cond2 in

    (match cond1',cond2' with
     | Ast.Expression(Ast.ConstantExpression(Ast.Bool(c1))), Ast.Expression(Ast.ConstantExpression(Ast.Bool(c2))) -> Ast.Expression(Ast.ConstantExpression(Ast.Bool(c1 || c2)))
     | c1, Ast.Not(c2) when c1=c2-> Ast.Expression(Ast.ConstantExpression(Ast.Bool(false)))
     | Ast.Not(c1), c2 when c1=c2-> Ast.Expression(Ast.ConstantExpression(Ast.Bool(false)))
     | Ast.Expression(Ast.ConstantExpression(Ast.Bool(false))), _ -> Ast.Expression(Ast.ConstantExpression(Ast.Bool(false)))
     | _, Ast.Expression(Ast.ConstantExpression(Ast.Bool(false))) -> Ast.Expression(Ast.ConstantExpression(Ast.Bool(false)))
     | Ast.Expression(Ast.ConstantExpression(Ast.Bool(true))), _ -> cond2'
     | _, Ast.Expression(Ast.ConstantExpression(Ast.Bool(true))) -> cond1'
     | _ when cond1'=cond2'-> cond1'
     | _ -> Ast.And(cond1', cond2'))












(* ajoute l'expression expr au graphe rtl, les pseudo registres utilisés sont contenus dans pseudo_reg 
   retourne rtl modifié, pseudo_reg modifié et le pseudo registre contenant expr *)
let rec convert_expression expr pseudo_reg rtl = 
  match expr with
  | Ast.VariableExpression(id) ->
    rtl, pseudo_reg, Dict.find id pseudo_reg.vars

  | Ast.ConstantExpression(cte) -> 
    let c = (match cte with
        | Ast.Int(i) -> i
        | Ast.Bool(b) -> if b then -1 else 0
      ) in
    let pseudo_reg', return_reg = new_register pseudo_reg in
    let rtl', _ = add_node (Node("li", PseudoReg(return_reg), Cte(c), Null)) rtl in
    rtl', pseudo_reg', return_reg

  | Ast.MinusExpression(expr1) -> 
    let rtl_expr1, pseudo_reg_expr1, return_reg_expr1 = convert_expression expr1 pseudo_reg rtl in
    let pseudo_reg', return_reg' = new_register pseudo_reg_expr1 in

    let rtl', _ = add_node (Node("neg", PseudoReg(return_reg'), PseudoReg(return_reg_expr1), Null)) rtl_expr1 in
    rtl', pseudo_reg', return_reg'

  | Ast.ArithmeticExpression(expr1, op, Ast.ConstantExpression(Ast.Int(c2))) -> 
    let rtl_expr1, pseudo_reg_expr1, return_reg_expr1 = convert_expression expr1 pseudo_reg rtl in
    let rtl_c2, pseudo_reg_c2, arg3, op' = (match op with
        | Ast.Plus ->
          rtl_expr1, pseudo_reg_expr1,(Cte(c2)), "addi"

        | Ast.Times ->
          let a,b = log2 c2 in
          if b = 1
          then rtl_expr1, pseudo_reg_expr1,(Cte(a)), "sll"
          else
            let _rtl, _pseudo_reg, _return_reg = convert_expression (Ast.ConstantExpression(Ast.Int(c2))) pseudo_reg_expr1 rtl_expr1
            in _rtl, _pseudo_reg, PseudoReg(_return_reg), "mul"

        | Ast.Minus ->
          rtl_expr1, pseudo_reg_expr1,(Cte(-1*c2)), "addi"

        | Ast.Divide ->
          let a,b = log2 c2 in
          if b = 1
          then rtl_expr1, pseudo_reg_expr1,(Cte(a)), "sra"
          else
            let _rtl, _pseudo_reg, _return_reg = convert_expression (Ast.ConstantExpression(Ast.Int(c2))) pseudo_reg_expr1 rtl_expr1
            in _rtl, _pseudo_reg, PseudoReg(_return_reg), "div"
      ) in
    let pseudo_reg', return_reg' = new_register pseudo_reg_c2 in

    let rtl', _ = add_node (Node(op', PseudoReg(return_reg'), PseudoReg(return_reg_expr1), arg3)) rtl_c2 in
    rtl', pseudo_reg', return_reg'

  | Ast.ArithmeticExpression(expr1, op, expr2) -> 
    let rtl_expr1, pseudo_reg_expr1, return_reg_expr1 = convert_expression expr1 pseudo_reg rtl in
    let rtl_expr2, pseudo_reg_expr2, return_reg_expr2 = convert_expression expr2 pseudo_reg_expr1 rtl_expr1 in
    let pseudo_reg', return_reg' = new_register pseudo_reg_expr2 in
    let op' = (match op with
        | Ast.Plus -> "add"
        | Ast.Times -> "mul"
        | Ast.Minus -> "sub"
        | Ast.Divide -> "div"
      ) in

    let rtl', _ = add_node (Node(op', PseudoReg(return_reg'), PseudoReg(return_reg_expr1), PseudoReg(return_reg_expr2))) rtl_expr2 in
    rtl', pseudo_reg', return_reg'

  | Ast.ComparaisonExpression(expr1, op, expr2) ->
    let rtl_expr1, pseudo_reg_expr1, return_reg_expr1 = convert_expression expr1 pseudo_reg rtl in
    let rtl_expr2, pseudo_reg_expr2, return_reg_expr2 = convert_expression expr2 pseudo_reg_expr1 rtl_expr1 in
    let pseudo_reg', return_reg' = new_register pseudo_reg_expr2 in
    let op' = (match op with
        | Ast.GT -> "sgt"
        | Ast.LT -> "slt"
        | Ast.GE -> "sge"
        | Ast.LE -> "sle"
        | Ast.EQ -> "seq"
        | Ast.NE -> "sne"
      ) in

    let rtl', _ = add_node (Node(op', PseudoReg(return_reg'), PseudoReg(return_reg_expr1), PseudoReg(return_reg_expr2))) rtl_expr2 in
    rtl', pseudo_reg', return_reg'

  | Ast.FunctionCallExpression(Ast.Id(id), expr_list) -> 
    let rec aux l rtl pseudo_reg l_reg = (
      match l with 
      | [] -> (rtl, pseudo_reg, l_reg)
      | a::b -> let rtl', pseudo_reg', return_reg' = convert_expression a pseudo_reg rtl in
        aux b rtl' pseudo_reg' (l_reg@[PseudoReg(return_reg')])
    ) in

    let rtl_expr_list, pseudo_reg_expr_list, l_reg_expr_list = aux expr_list rtl pseudo_reg [] in
    let pseudo_reg', return_reg' = new_register pseudo_reg_expr_list in

    let rtl', _ = add_node (FunctionCallNode(PseudoReg(return_reg'), id, l_reg_expr_list)) rtl_expr_list in
    rtl', pseudo_reg', return_reg'

  | Ast.ArrayAccessExpression(expr1, expr2) ->
    let address = reduce_expression (Ast.ArithmeticExpression(Ast.ArithmeticExpression(expr2, Ast.Times, Ast.ConstantExpression(Ast.Int(4))), Ast.Plus, expr1)) in

    let rtl_address, pseudo_reg_address, return_reg_address = convert_expression address pseudo_reg rtl in

    let pseudo_reg', return_reg' = new_register pseudo_reg_address in
    let rtl', _ = add_node (Node("lw", PseudoReg(return_reg'), Address(0, return_reg_address), Null)) rtl_address in

    rtl', pseudo_reg', return_reg'

  | Ast.NewArrayExpression(_, expr1) -> 
    let rtl_expr1, pseudo_reg_expr1, return_reg_expr1 = convert_expression expr1 pseudo_reg rtl in
    let pseudo_reg_sll, return_reg_sll = new_register pseudo_reg_expr1 in
    let rtl_sll, _ = add_node (Node("sll", PseudoReg(return_reg_sll), PseudoReg(return_reg_expr1), Cte(2))) rtl_expr1 in
    let pseudo_reg', return_reg' = new_register pseudo_reg_sll in

    let rtl', _ = add_node (FunctionCallNode(PseudoReg(return_reg'), "malloc", [PseudoReg(return_reg_sll)])) rtl_sll in
    rtl', pseudo_reg', return_reg'


(* ajoute la condition cond au graphe rtl, les pseudo registres utilisés sont contenus dans pseudo_reg 
   retourne rtl modifié, pseudo_reg modifié et le pseudo registre contenant cond *)
let rec convert_condition cond pseudo_reg rtl = 
  match cond with
  | Ast.Expression(expr) ->
    convert_expression expr pseudo_reg rtl

  | Ast.Not(Ast.Or(cond1, cond2)) ->
    let rtl_cond1, pseudo_reg_cond1, return_reg_cond1 = convert_condition cond1 pseudo_reg rtl in
    let rtl_cond2, pseudo_reg_cond2, return_reg_cond2 = convert_condition cond2 pseudo_reg_cond1 rtl_cond1 in
    let pseudo_reg', return_reg' = new_register pseudo_reg_cond2 in

    let rtl', _ = add_node (Node("nor", PseudoReg(return_reg'), PseudoReg(return_reg_cond1), PseudoReg(return_reg_cond2))) rtl_cond2 in
    rtl', pseudo_reg', return_reg'

  | Ast.Not(cond1) ->
    let rtl_cond1, pseudo_reg_cond1, return_reg_cond1 = convert_condition cond1 pseudo_reg rtl in
    let pseudo_reg', return_reg' = new_register pseudo_reg_cond1 in

    let rtl', _ = add_node (Node("not", PseudoReg(return_reg'), PseudoReg(return_reg'), Null)) rtl_cond1 in
    rtl', pseudo_reg', return_reg'

  | Ast.Or(cond1, cond2) ->
    let rtl_cond1, pseudo_reg_cond1, return_reg_cond1 = convert_condition cond1 pseudo_reg rtl in
    let rtl_cond2, pseudo_reg_cond2, return_reg_cond2 = convert_condition cond2 pseudo_reg_cond1 rtl_cond1 in
    let pseudo_reg', return_reg' = new_register pseudo_reg_cond2 in

    let rtl', _ = add_node (Node("or", PseudoReg(return_reg'), PseudoReg(return_reg_cond1), PseudoReg(return_reg_cond2))) rtl_cond2 in
    rtl', pseudo_reg', return_reg'

  | Ast.And(cond1, cond2) ->
    let rtl_cond1, pseudo_reg_cond1, return_reg_cond1 = convert_condition cond1 pseudo_reg rtl in
    let rtl_cond2, pseudo_reg_cond2, return_reg_cond2 = convert_condition cond2 pseudo_reg_cond1 rtl_cond1 in
    let pseudo_reg', return_reg' = new_register pseudo_reg_cond2 in

    let rtl', _ = add_node (Node("and", PseudoReg(return_reg'), PseudoReg(return_reg_cond1), PseudoReg(return_reg_cond2))) rtl_cond2 in
    rtl', pseudo_reg', return_reg'



let add_cond_node cond pseudo_reg rtl =
  let rtl_cond, pseudo_reg_cond, return_reg_cond = convert_condition cond pseudo_reg rtl in

  let line_end_cond = if RTL.is_empty rtl_cond then 0 else fst (RTL.max_binding rtl_cond) in
  let RTLNode(node_end_cond, _, _) = if RTL.is_empty rtl_cond then RTLNode(Node("",Null,Null,Null),0,None) else RTL.find line_end_cond rtl_cond
  in

  match node_end_cond with
  | Node("seq", _, arg1, Cte(0))
  | Node("seq", _, Cte(0), arg1) -> Node("beqz", arg1, Null, Null), rtl_cond, line_end_cond, pseudo_reg_cond

  | Node("sne", _, arg1, Cte(0))
  | Node("sne", _, Cte(0), arg1) -> Node("bnez", arg1, Null, Null), rtl_cond, line_end_cond, pseudo_reg_cond

  | Node("sgt", _, arg1, Cte(0))
  | Node("slt", _, Cte(0), arg1) -> Node("bgtz", arg1, Null, Null), rtl_cond, line_end_cond, pseudo_reg_cond

  | Node("slt", _, arg1, Cte(0))
  | Node("sgt", _, Cte(0), arg1) -> Node("bltz", arg1, Null, Null), rtl_cond, line_end_cond, pseudo_reg_cond

  | Node("sge", _, arg1, Cte(0))
  | Node("sle", _, Cte(0), arg1) -> Node("bgez", arg1, Null, Null), rtl_cond, line_end_cond, pseudo_reg_cond

  | Node("sle", _, arg1, Cte(0))
  | Node("sge", _, Cte(0), arg1) -> Node("blez", arg1, Null, Null), rtl_cond, line_end_cond, pseudo_reg_cond

  | Node("seq", _, arg1, arg2) -> Node("beq", arg1, arg2, Null), rtl_cond, line_end_cond, pseudo_reg_cond

  | Node("sne", _, arg1, arg2) -> Node("bne", arg1, arg2, Null), rtl_cond, line_end_cond, pseudo_reg_cond

  | Node("sgt", _, arg1, arg2) -> Node("bgt", arg1, arg2, Null), rtl_cond, line_end_cond, pseudo_reg_cond

  | Node("slt", _, arg1, arg2) -> Node("blt", arg1, arg2, Null), rtl_cond, line_end_cond, pseudo_reg_cond

  | Node("sge", _, arg1, arg2) -> Node("bge", arg1, arg2, Null), rtl_cond, line_end_cond, pseudo_reg_cond

  | Node("sle", _, arg1, arg2) -> Node("ble", arg1, arg2, Null), rtl_cond, line_end_cond, pseudo_reg_cond

  | _ -> let rtl_test_cond_temp, line_test_cond = add_node (Node("", Null, Null, Null)) rtl_cond in
    Node("bnez", PseudoReg(return_reg_cond), Null, Null), rtl_test_cond_temp, line_test_cond, pseudo_reg_cond





(* ajoute l'instruction statement au graphe rtl, les pseudo registres utilisés sont contenus dans pseudo_reg 
   retourne rtl modifié et pseudo_reg modifié *)
let rec convert_statement statement pseudo_reg rtl = 
  match statement with
  | Ast.ProcedureCallStatement(Ast.Id(id), expr_list) ->
    let rec aux l rtl pseudo_reg l_reg = (
      match l with 
      | [] -> (rtl, pseudo_reg, l_reg)
      | a::b -> let rtl', pseudo_reg', return_reg' = convert_expression (reduce_expression a) pseudo_reg rtl in
        aux b rtl' pseudo_reg' (l_reg@[PseudoReg(return_reg')])
    ) in

    let rtl_expr_list, pseudo_reg_expr_list, l_reg_expr_list = aux expr_list rtl pseudo_reg [] in

    let rtl', _ = add_node (FunctionCallNode(Null, id, l_reg_expr_list)) rtl_expr_list in
    rtl', pseudo_reg_expr_list


  | Ast.AssignVarStatement(id, expr) -> 
    let expr' = reduce_expression expr in
    let rtl_expr, pseudo_reg_expr, return_reg_expr = convert_expression expr' pseudo_reg rtl in

    (match expr' with
     | Ast.VariableExpression(_) ->     
       let rtl', _ = add_node (Node("move", PseudoReg(Dict.find id pseudo_reg_expr.vars), PseudoReg(return_reg_expr), Null)) rtl_expr in
       rtl', pseudo_reg_expr

     | _ -> 
       let line_expr = fst (RTL.max_binding rtl_expr) in

       let node_expr = (match RTL.find line_expr rtl_expr with
           | RTLNode(Node(op, arg1, arg2, arg3),l1,l2) ->  Node(op, PseudoReg(Dict.find id pseudo_reg_expr.vars), arg2, arg3)
           | RTLNode(FunctionCallNode(arg, function_id, arg_list),l1,l2) ->  FunctionCallNode(PseudoReg(Dict.find id pseudo_reg_expr.vars), function_id, arg_list)
         ) in

       let rtl', _ = add_rtl_node line_expr node_expr (1+line_expr)  None rtl_expr in
       rtl', {max_reg= pseudo_reg_expr.max_reg -1;vars=(Dict.remove (Ast.Id(string_of_int return_reg_expr)) pseudo_reg_expr.vars)}
    )

  | Ast.AssignArrayStatement(expr1, expr2, expr3) -> 
    let rtl_expr3, pseudo_reg_expr3, return_reg_expr3 = convert_expression (reduce_expression expr3) pseudo_reg rtl in

    let address = reduce_expression (Ast.ArithmeticExpression(Ast.ArithmeticExpression(expr2, Ast.Times, Ast.ConstantExpression(Ast.Int(4))), Ast.Plus, expr1)) in

    let rtl_address, pseudo_reg_address, return_reg_address = convert_expression address pseudo_reg_expr3 rtl_expr3 in

    let pseudo_reg', return_reg' = new_register pseudo_reg_address in
    let rtl', _ = add_node (Node("sw", PseudoReg(return_reg_expr3), Address(0, return_reg_address), Null)) rtl_address in

    rtl', pseudo_reg'

  | Ast.IfStatement(cond, statement1, statement2) -> 
    let cond' = reduce_condition cond in
    if cond' = Ast.Expression(Ast.ConstantExpression(Ast.Bool(true)))
    then convert_statement statement1 pseudo_reg rtl
    else if cond' = Ast.Expression(Ast.ConstantExpression(Ast.Bool(false)))
    then convert_statement statement2 pseudo_reg rtl
    else
      let node_cond, rtl_cond_temp, line_cond, pseudo_reg_cond_temp = add_cond_node cond' pseudo_reg rtl in

      let rtl_statement2, pseudo_reg_statement2 = convert_statement statement2 pseudo_reg_cond_temp rtl_cond_temp in

      let line_end_else = fst (RTL.max_binding rtl_statement2) in
      let RTLNode(node_end_else, _, _) = RTL.find line_end_else rtl_statement2 in

      let rtl_statement1, pseudo_reg_statement1 = convert_statement statement1 pseudo_reg_statement2 rtl_statement2 in

      let line_end_then = fst (RTL.max_binding rtl_statement1) in
      let RTLNode(node_end_then, _, _) = RTL.find line_end_then rtl_statement1 in

      let rtl_test_cond, _ = add_rtl_node line_cond node_cond (1+line_end_else) (Some(1+line_cond)) rtl_statement1 in
      let rtl_end_else, _ = add_rtl_node line_end_else node_end_else (1+line_end_then) None rtl_test_cond in
      let rtl', _ = add_rtl_node line_end_then node_end_then (1+line_end_then)  None rtl_end_else in
      rtl', pseudo_reg_statement1

  | Ast.WhileStatement(cond, statement1) ->
    let cond' = reduce_condition (Ast.Not(cond)) in
    if cond' = Ast.Expression(Ast.ConstantExpression(Ast.Bool(false)))
    then rtl, pseudo_reg
    else 
      let line_begin_cond = if RTL.is_empty rtl then -1 else fst (RTL.max_binding rtl) in

      let node_cond, rtl_cond_temp, line_cond, pseudo_reg_cond_temp = add_cond_node cond' pseudo_reg rtl in

      let rtl_statement1, pseudo_reg_statement1 = convert_statement statement1 pseudo_reg_cond_temp rtl_cond_temp in

      let line_end_loop = fst (RTL.max_binding rtl_statement1) in
      let RTLNode(node_end_loop, _, _) = RTL.find line_end_loop rtl_statement1 in

      let rtl_test_cond, _ = add_rtl_node line_cond node_cond (line_end_loop+1) (Some(1+line_cond)) rtl_statement1 in
      let rtl', _ = add_rtl_node line_end_loop node_end_loop (1+line_begin_cond) None rtl_test_cond in
      rtl', pseudo_reg_statement1

  | Ast.BlocStatement(list_statement) ->
    let rec aux l rtl reg = (
      match l with 
      | [] -> (rtl, reg)
      | a::b -> let rtl', reg' = convert_statement a reg rtl in
        aux b rtl' reg'
    ) in aux list_statement rtl pseudo_reg















(* affiche le noeud RTLNode(...) avec le label n_line *)
let print_rtl_node n_line (RTLNode(node, label1, label2)) =
  match node with
  | Node(op, arg1, arg2, arg3) -> 
    Printf.printf "%i: %s" n_line op;
    (match arg1 with
     | PseudoReg(arg) -> Printf.printf ", %%%i" arg
     | Cte(arg) -> Printf.printf ", %i" arg
     | Address(a, b) -> Printf.printf ", %i(%%%i)" a b
     | _ -> ());
    (match arg2 with
     | PseudoReg(arg) -> Printf.printf ", %%%i" arg
     | Cte(arg) -> Printf.printf ", %i" arg
     | Address(a, b) -> Printf.printf ", %i(%%%i)" a b
     | _ -> ());
    (match arg3 with
     | PseudoReg(arg) -> Printf.printf ", %%%i" arg
     | Cte(arg) -> Printf.printf ", %i" arg
     | Address(a, b) -> Printf.printf ", %i(%%%i)" a b
     | _ -> ());
    Printf.printf " --> %i" label1;
    (match label2 with
     | Some(label) -> Printf.printf ", %i" label
     | _ -> ());
    Printf.printf "\n"

  | FunctionCallNode(arg, id, arg_list) -> 
    Printf.printf "%i: call " n_line;
    (match arg with
     | PseudoReg(return_reg) -> Printf.printf "%%%i, " return_reg
     | _ -> ());
    Printf.printf "%s(" id;
    print_reg_list arg_list;
    Printf.printf ") --> %i\n" label1

(* affiche la fonction rtl FunctionRTL(...) *)
let print_rtl_function (FunctionRTL(id, arg_list, return_reg, reg_list, entry, exit, rtl)) =
  (if return_reg = None
   then Printf.printf "procedure %s(" id
   else Printf.printf "function %s(" id);

  print_reg_list arg_list;

  (match return_reg with
   | Some(PseudoReg(arg)) -> Printf.printf ") : %%%i\nvar " arg
   | _ -> Printf.printf ")\nvar ");

  print_reg_list reg_list;

  Printf.printf "\nentry %i\nexit %i\n" entry exit;

  RTL.iter print_rtl_node rtl





(* affiche le graphe rtl l *)
let rec print_rtl l =
  match l with
  | [] -> ()
  | a::b ->
    print_rtl_function a;
    Printf.printf "\n";
    print_rtl b








(* crée le graphe rtl à partir du programme Ast.Program(...) *)
let build_rtl (Ast.Program(decl_list, statement)) =
  let rec aux decl_l vars fcts = 
    match decl_l with
    | [] -> vars, fcts
    | Ast.VarDeclaration(Ast.Environment(env))::b ->
      aux b (init_dict env vars) fcts

    | Ast.ProcedureDeclaration(Ast.Id(id), Ast.Environment(env), d_l, statement)::b -> 
      let dict = init_dict env vars in
      let dict', _ = aux d_l dict [] in
      let rtl, reg = convert_statement statement {vars= dict'; max_reg= Dict.cardinal dict'} RTL.empty in
      aux b vars (FunctionRTL(id, get_env_reg env dict', None, get_reg_from_dict reg.vars, 0, RTL.cardinal rtl, rtl)::fcts)

    | Ast.FunctionDeclaration(Ast.Id(id), Ast.Environment(env), t, d_l, statement)::b ->
      let dict_env_vars = init_dict env vars in
      let dict_env_vars_dl, _ = aux d_l dict_env_vars [] in
      let dict' = Dict.add (Ast.Id(id)) 0 dict_env_vars_dl in
      let rtl, reg = convert_statement statement {vars= dict'; max_reg= Dict.cardinal dict'-1} RTL.empty in
      aux b vars (FunctionRTL(id, get_env_reg env dict', Some(PseudoReg(0)), get_reg_from_dict reg.vars, 0, RTL.cardinal rtl, rtl)::fcts)

  in

  let vars, fcts = aux decl_list Dict.empty [] in
  let rtl, reg = convert_statement statement {vars= vars; max_reg= Dict.cardinal vars} RTL.empty in
  FunctionRTL("_main", [], None, get_reg_from_dict reg.vars, 0, RTL.cardinal rtl, rtl)::fcts